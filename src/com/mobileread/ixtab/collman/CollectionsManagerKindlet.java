package com.mobileread.ixtab.collman;

import ixtab.jailbreak.Jailbreak;
import ixtab.jailbreak.SuicidalKindlet;

import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.security.AllPermission;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import com.amazon.kindle.kindlet.KindletContext;
import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.firmware.UnsupportedFirmwareException;

public class CollectionsManagerKindlet extends SuicidalKindlet {

	private static final String SPLASH_TEXT = "www.ixtab.tk";
	private static final int SPLASH_DISPLAY_MINDURATION_MS = 0;
	
	private KindletContext context;
	private CollectionsManager ui;
	private JLabel centerLabel;

	protected Jailbreak instantiateJailbreak() {
		return new LocalJailbreak();
	}

	public void onCreate(KindletContext kindletContext) {
		this.context = kindletContext;
		this.centerLabel = new JLabel(SPLASH_TEXT);
		initUi();
	}

	protected void onStart() {
		if (ui == null) {
			new Thread() {
				public void run() {
					try {
						while (!(context.getRootContainer().isValid() && context
								.getRootContainer().isVisible())) {
							Thread.sleep(100);
						}
						Thread.sleep(SPLASH_DISPLAY_MINDURATION_MS);
					} catch (Exception e) {
					}
					;
					SwingUtilities.invokeLater(new Runnable() {
						public void run() {
							initUiAfterSplash();
						}
					});
				}
			}.start();
		}
	}

	protected void onStop() {
		if (ui != null) {
			ui.onStop();
		}
	}

	private void initUiAfterSplash() {
		try {
			if (jailbreak.isAvailable()) {
				if (((LocalJailbreak) jailbreak).requestPermissions()) {
					context.getRootContainer().removeAll();
					ui = AdapterConfiguration.getInstance()
							.getCollectionManager(context);
					ui.init();
				} else {
					String title = "Mobileread Kindlet Kit Failed";
					setCentralMessage(title);
					String error = "The Mobileread Kindlet Kit failed to obtain all required permissions. Please report this error.";
					showAndQuit(title, error, null);
				}
			} else {
				String title = "Mobileread Kindlet Kit Required";
				String message = "This application requires the Mobileread Kindlet Kit be installed. Please install the MobileRead Kindlet Kit before using this application.";
				setCentralMessage(title);
				showAndQuit(title, message, null);
			}
		} catch (UnsupportedFirmwareException fwex) {
			String title = "Unsupported Firmware";
			String message = "Your Kindle is running firmware "
					+ fwex.getVersion()
					+ ", which is not supported by this version of Collections Manager.";
			setCentralMessage(title);
			showAndQuit(title, message, null);
		} catch (Throwable t) {
			String title = "Unexpected error";
			String message = "An unexpected error has occurred while starting Collections Manager. Please report this error to the author. The application will now crash, and you can find the stacktrace in developer/Collections Manager/work/crash.log";
			setCentralMessage(title);
			showAndQuit(title, message, t);
		}
	}

	private void showAndQuit(String title, String message, Throwable crash) {
		KOptionPane.showMessageDialog(context.getRootContainer(), message,
				title);
		if (crash != null) {
			throw new RuntimeException(crash);
		}
		try {
			Runtime.getRuntime()
					.exec("lipc-set-prop com.lab126.appmgrd stop app://com.lab126.booklet.kindlet");
		} catch (Throwable ignored) {
		}
	}

	private void setCentralMessage(String centered) {
		centerLabel.setText(centered);
		context.getRootContainer().validate();
	}

	private void initUi() {
		Container pane = context.getRootContainer();
		pane.removeAll();
		centerLabel.setFont(new Font(centerLabel.getFont().getName(),
				Font.BOLD, centerLabel.getFont().getSize() + 6));

		pane.setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.fill |= GridBagConstraints.VERTICAL;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;

		pane.add(centerLabel, gbc);
	}

	private static class LocalJailbreak extends Jailbreak {

		public boolean requestPermissions() {
			boolean ok = getContext().requestPermission(new AllPermission());
			return ok;
		}

	}

}