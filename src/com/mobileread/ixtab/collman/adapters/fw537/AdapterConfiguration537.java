package com.mobileread.ixtab.collman.adapters.fw537;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration537 extends AdapterConfiguration {

	public AdapterConfiguration537() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter537();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter537();
	}

	public CatalogService getCatalogService() {
		return new CatalogService537();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager537(context);
	}

}
