package com.mobileread.ixtab.collman.adapters.fw540;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration540 extends AdapterConfiguration {

	public AdapterConfiguration540() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter540();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter540();
	}

	public CatalogService getCatalogService() {
		return new CatalogService540();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager540(context);
	}

}
