package com.mobileread.ixtab.collman.adapters.fw532;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration532 extends AdapterConfiguration {

	public AdapterConfiguration532() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter532();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter532();
	}

	public CatalogService getCatalogService() {
		return new CatalogService532();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager532(context);
	}

}
