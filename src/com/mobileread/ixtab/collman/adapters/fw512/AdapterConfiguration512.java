package com.mobileread.ixtab.collman.adapters.fw512;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration512 extends AdapterConfiguration {

	public AdapterConfiguration512() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter512();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter512();
	}

	public CatalogService getCatalogService() {
		return new CatalogService512();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager512(context);
	}

}
