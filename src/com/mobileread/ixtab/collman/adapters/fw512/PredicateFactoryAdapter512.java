package com.mobileread.ixtab.collman.adapters.fw512;

import com.amazon.kindle.content.catalog.PredicateFactory;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class PredicateFactoryAdapter512 implements PredicateFactoryAdapter {
	
	private Predicate wrap(Object wrapped) {
		return new Predicate(wrapped);
	}

	private com.amazon.kindle.content.catalog.Predicate[] explode(
			Predicate[] in) {
		if (in == null) {
			return null;
		}
		com.amazon.kindle.content.catalog.Predicate[] out = new com.amazon.kindle.content.catalog.Predicate[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (com.amazon.kindle.content.catalog.Predicate) in[i].delegate;
		}
		return out;
	}

	public Predicate startsWith(String key, String value) {
		return wrap(PredicateFactory.startsWith(key, value));
	}

	public Predicate and(Predicate[] predicates) {
		return wrap(PredicateFactory.and(explode(predicates)));
	}

	public Predicate isTrue(String what) {
		return wrap(PredicateFactory.isTrue(what));
	}

	public Predicate not(Predicate pred) {
		return wrap(PredicateFactory.not((com.amazon.kindle.content.catalog.Predicate) pred.delegate));
	}

	public Predicate equals(String key, String value) {
		return wrap(PredicateFactory.equals(key, value));
	}

	public Predicate or(Predicate[] predicates) {
		return wrap(PredicateFactory.or(explode(predicates)));
	}

	public Predicate notNull(String key) {
		return wrap(PredicateFactory.notNull(key));
	}

	public Predicate inList(String key, Object membersArray) {
		return wrap(PredicateFactory.inList(key, CatalogAdapter512.asUUIDArray((Object[]) membersArray)));
	}

	public Predicate greater(String key, long value, boolean inclusive) {
		return wrap(PredicateFactory.greater(key, value, inclusive));
	}

}
