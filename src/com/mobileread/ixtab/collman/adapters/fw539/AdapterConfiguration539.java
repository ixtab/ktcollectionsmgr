package com.mobileread.ixtab.collman.adapters.fw539;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration539 extends AdapterConfiguration {

	public AdapterConfiguration539() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter539();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter539();
	}

	public CatalogService getCatalogService() {
		return new CatalogService539();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager539(context);
	}

}
