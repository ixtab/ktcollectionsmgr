package com.mobileread.ixtab.collman.adapters.fw539;

import java.util.Date;

import com.amazon.ebook.util.a.g;
import com.amazon.ebook.util.text.M;
import com.amazon.kindle.content.catalog.f;
import com.amazon.kindle.content.catalog.CatalogEntry;
import com.amazon.kindle.content.catalog.CatalogEntryCollection;
import com.amazon.kindle.content.catalog.CatalogItem;
import com.amazon.kindle.content.catalog.MutableCollection;
import com.amazon.kindle.content.catalog.MutableEntry;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;

public class CatalogAdapter539 extends CatalogAdapter {

	public void setMembers(MutableCollection mutable, Object[] uuids) {
		g[] ids = asUUIDArray(uuids);
		mutable.Wj(ids); // line 17
	}

	protected static g[] asUUIDArray(Object[] in) {
		if (in == null) {
			return null;
		}
		g[] out = new g[in.length];
		for (int i=0; i < in.length; ++i) {
			out[i] = (g) in[i];
		}
		return out;
	}

	public void addMember(MutableCollection mutable, Object uuid) {
		mutable.cH((g) uuid); //19
	}

	public void setIsVisibleInRoot(MutableCollection mutable,
			boolean visible) {
		mutable.DE(visible); //44
	}

	public void setTitle(MutableCollection mutable, String title) {
		mutable.Wf(new M[] {new M(title)}); //26
	}

	public Object getUUID(CatalogEntry entry) {
		return entry.AG(); // 19
	}

	public String getLocation(CatalogEntry entry) {
		return entry.qg(); // 45
	}

	public String getTitle(CatalogEntry entry) {
		M[] titles = entry.Ef(); // 51
		return (titles == null || titles.length < 1) ? "" : titles[0].toString();
	}

	public String getCDEKey(CatalogItem item) {
		return item.af(); // 20
	}

	public String getCDEType(CatalogItem item) {
		return item.CF(); // 22
	}

	public Date getLastAccessDate(CatalogEntryCollection collection) {
		return collection.Kg(); // 43
	}

	public boolean isVisibleInHome(CatalogEntry entry) {
		return entry.Nf(); // 39
	}

	public void setIsVisibleInHome(MutableEntry mutable, boolean visible) {
		mutable.DE(visible); // 44
	}

	public int countParents(CatalogEntry entry) {
		return entry.Be(); // 27
	}

	public Object[] getParents(CatalogEntry entry) {
		return entry.zD(); // 25
	}

	public String getFirstCreditAsJSON(CatalogEntry entry) {
		f[] credits = entry.IF(); //21
		if (credits == null || credits.length < 1) {
			return null;
		}
		return credits[0].toJSONString();
	}

	public Object[] getMembers(CatalogEntryCollection collection) {
		return collection.rH(); // 17
	}

	public int countMembers(CatalogEntryCollection collection) {
		return collection.Ch(); // 19
	}

	public void setTitle(MutableEntry mutable, String title) {
		mutable.Wf(new M[] {new M(title)}); // 26
	}

}
