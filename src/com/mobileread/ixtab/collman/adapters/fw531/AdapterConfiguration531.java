package com.mobileread.ixtab.collman.adapters.fw531;

import com.amazon.kindle.kindlet.KindletContext;
import com.mobileread.ixtab.collman.CollectionsManager;
import com.mobileread.ixtab.collman.adapters.AdapterConfiguration;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogService;
import com.mobileread.ixtab.collman.adapters.PredicateFactoryAdapter;

public class AdapterConfiguration531 extends AdapterConfiguration {

	public AdapterConfiguration531() {
		
	};
	
	public PredicateFactoryAdapter getPredicateFactoryAdapter() {
		return new PredicateFactoryAdapter531();
	}

	public CatalogAdapter getCatalogAdapter() {
		return new CatalogAdapter531();
	}

	public CatalogService getCatalogService() {
		return new CatalogService531();
	}

	public CollectionsManager getCollectionManager(KindletContext context) {
		return new CollectionManager531(context);
	}

}
