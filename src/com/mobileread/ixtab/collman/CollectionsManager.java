package com.mobileread.ixtab.collman;

import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.JPanel;

import com.amazon.kindle.booklet.BookletContext;
import com.amazon.kindle.kindlet.KindletContext;
import com.amazon.kindle.kindlet.security.SecureStorage;
import com.amazon.kindle.restricted.device.impl.ChromeImplementation;
import com.amazon.kindle.restricted.runtime.Framework;
import com.mobileread.ixtab.collman.catalog.Catalog;
import com.mobileread.ixtab.collman.ui.MainPanel;
import com.mobileread.ixtab.collman.ui.menu.KindletMenu;
import com.mobileread.ixtab.collman.ui.menu.MainMenu;

public abstract class CollectionsManager extends JPanel {

	private static final long serialVersionUID = 1L;

	protected final KindletContext context;
	protected BookletContext bookletContext;
	protected ChromeImplementation chromeImplementation;

	public CollectionsManager(KindletContext context) {
		this.context = context;
		bookletContext = getBookletContext();
		chromeImplementation = getChromeImplementation();
		Settings.init((SecureStorage) context.getService(SecureStorage.class));
		addMenu();
		modifyToolbar();
		Catalog.init(isSearchSupported());
		Clipboard.init();
	}

	protected abstract boolean isSearchSupported();

	public void init() {
		this.setLayout(new BorderLayout());
		
		this.add(Status.label, BorderLayout.NORTH);
		
		JPanel mainPanel = new MainPanel();
		this.add(mainPanel, BorderLayout.CENTER);
		
		// simple way to bring buttons into right state
		if (Settings.get().isHideLowerPanel()) {
			Event.post(Event.LOWER_PANEL_DISABLE, this, PanelPosition.BOTH);
		}
		Event.post(Event.STARTUP, this, PanelPosition.BOTH);

		Container root = context.getRootContainer();
		root.setLayout(new BorderLayout());
		root.add(this, BorderLayout.CENTER);
		root.validate();
	}

	protected abstract BookletContext getBookletContext();

	private ChromeImplementation getChromeImplementation() {
		return (ChromeImplementation) Framework
				.getService(com.amazon.kindle.booklet.ChromeHeaderBar.class);
	}

	protected abstract void modifyToolbar();

	private void addMenu() {
		MainMenu main = new MainMenu();
		KindletMenu.instantiate(context, main);
	}

	public abstract void onStop();
}
