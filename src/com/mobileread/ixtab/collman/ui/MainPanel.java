package com.mobileread.ixtab.collman.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

import com.mobileread.ixtab.collman.Mouse;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.actions.SyncPanelsAction;

public class MainPanel extends JPanel implements Mouse.Sensitive {
	private static final long serialVersionUID = 1L;

	private final BrowsePanel upper, lower;

	public MainPanel() {
		setBorder(Borders.WHITE5);
		setLayout(new GridBagLayout());

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;

		upper = new BrowsePanel(PanelPosition.UPPER);
		lower = new BrowsePanel(PanelPosition.LOWER);

		add(upper, gbc);
		add(new ActionsPanel(upper, lower), gbc);
		add(lower, gbc);

		Mouse.initialize(this);
	}

	public boolean handleMouseReleased(Mouse.State state) {
		if (state.isSwipeToSouth()) {
			if (getComponentAt(state.firstPressed.getPoint()) == upper
					&& getComponentAt(state.released.getPoint()) == lower) {
				new SyncPanelsAction(SyncPanelsAction.DIRECTION_UPPER_TO_LOWER)
						.perform();
				return true;
			}
		} else if (state.isSwipeToNorth()) {
			if (getComponentAt(state.firstPressed.getPoint()) == lower
					&& getComponentAt(state.released.getPoint()) == upper) {
				new SyncPanelsAction(SyncPanelsAction.DIRECTION_LOWER_TO_UPPER)
						.perform();
				return true;
			}
		}
		return false;
	}

}
