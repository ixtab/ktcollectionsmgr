package com.mobileread.ixtab.collman.ui.model;

import java.util.List;

import javax.swing.DefaultComboBoxModel;

public class ListBackedComboboxModel extends DefaultComboBoxModel {
	private static final long serialVersionUID = 1L;
	private final List backend;
	
	private transient boolean updating = false;
	
	public boolean isUpdating() {
		return updating;
	}

	public ListBackedComboboxModel(List backend) {
		this.backend = backend;
	}

	public synchronized void syncToBackend() {
		updating = true;
		this.removeAllElements();
		for (int i = 0; i < backend.size(); ++i) {
			this.addElement(backend.get(i));
		}
		updating = false;
	}
}
