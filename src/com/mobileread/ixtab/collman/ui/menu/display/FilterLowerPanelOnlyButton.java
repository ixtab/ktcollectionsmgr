package com.mobileread.ixtab.collman.ui.menu.display;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.Settings;
import com.mobileread.ixtab.collman.actions.ToggleFilterLowerPanelOnlyAction;

public class FilterLowerPanelOnlyButton extends JCheckBox implements ActionListener {
	private static final long serialVersionUID = 1L;

	public FilterLowerPanelOnlyButton() {
		super(new ToggleFilterLowerPanelOnlyAction());
		setSelected(Settings.get().isFilterLowerPanelOnly());
		Event.addListener(this);
	}
	
	public void actionPerformed(ActionEvent event) {
		Event e = (Event) event;
		if (e.getID() == Event.FILTER_LOWER_ONLY_OFF) {
			setSelected(false);
		} else if (e.getID() == Event.FILTER_LOWER_ONLY_ON) {
			setSelected(true);
		}
	}
}

