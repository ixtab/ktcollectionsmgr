package com.mobileread.ixtab.collman;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import com.mobileread.ixtab.collman.catalog.Entry;

public class Clipboard implements ActionListener {
	
	private static final int UPPER = 0;
	private static final int LOWER = 1;

	private static Clipboard instance = new Clipboard();
	
	private final List[] items = new List[2];
	private boolean lowerEnabled = true;
	
	public static Clipboard get() {
		return instance;
	}
	
	public static void init() {
		
	}
	
	private Clipboard() {
		items[UPPER] = new ArrayList();
		items[LOWER] = new ArrayList();
		Event.addListener(this);
	}

	public synchronized void actionPerformed(ActionEvent event) {
		int previousCount[] = new int[] {items[UPPER].size(), items[LOWER].size()};
		Event e = (Event) event;
		switch(e.getID()) {
		case Event.VIEW_ENTRY_SELECTED:
			items[getAffectedIndex(e)].add(e.getSource());
			break;
		case Event.VIEW_ENTRY_UNSELECTED:
			items[getAffectedIndex(e)].remove(e.getSource());
			break;
		case Event.VIEW_RESET:
			items[getAffectedIndex(e)].clear();
			break;
		case Event.LOWER_PANEL_DISABLE:
			lowerEnabled = false;
			break;
		case Event.LOWER_PANEL_ENABLE:
			lowerEnabled = true;
			break;
			
		default:
		}
		int newCount[] = new int[] {items[UPPER].size(), items[LOWER].size()};
		postEventIfChanged(previousCount, newCount);
	}

	private int getAffectedIndex(Event e) {
		return e.getPosition()-1;
	}

	private void postEventIfChanged(int[] previousCount, int[] newCount) {
		int event = 0;
		if (previousCount[UPPER] != newCount[UPPER]) {
			event |= PanelPosition.UPPER;
		}
		if (previousCount[LOWER] != newCount[LOWER]) {
			event |= PanelPosition.LOWER;
		}
		if (event != 0) {
			Event.post(Event.CLIPBOARD_CHANGED, this, event);
		}
	}

	public int countItems(int positions) {
		int result = UPPER;
		if (concernsUpperItems(positions)) {
			result += items[UPPER].size();
		}
		if (concernsLowerItems(positions)) {
			result += items[LOWER].size();
		}
		return result;
	}

	private boolean concernsLowerItems(int positions) {
		return lowerEnabled && (positions & PanelPosition.LOWER) != 0;
	}

	private boolean concernsUpperItems(int positions) {
		return (positions & PanelPosition.UPPER) != 0;
	}

	public Entry[] getItems(int positions) {
		List result = new ArrayList();
		if (concernsUpperItems(positions)) {
			result.addAll(items[UPPER]);
		}
		if (concernsLowerItems(positions)) {
			result.addAll(items[LOWER]);
		}
		Entry[] entries = new Entry[result.size()];
		for (int i=0; i < entries.length; ++i) {
			entries[i] = (Entry) result.get(i);
		}
		return entries;
	}
}
