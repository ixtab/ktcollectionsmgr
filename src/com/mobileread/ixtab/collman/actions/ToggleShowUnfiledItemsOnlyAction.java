package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.mobileread.ixtab.collman.Event;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.PanelPosition;
import com.mobileread.ixtab.collman.Settings;

public class ToggleShowUnfiledItemsOnlyAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	public ToggleShowUnfiledItemsOnlyAction() {
		super(I18n.get().i18n(I18n.SHOW_UNFILED_ONLY_KEY, I18n.SHOW_UNFILED_ONLY_VALUE));
	}

	public void actionPerformed(ActionEvent e) {
		Event.post(Settings.get().isShowUnfiledItemsOnly() ? Event.SHOW_UNFILED_ONLY_OFF : Event.SHOW_UNFILED_ONLY_ON, this, PanelPosition.BOTH);
	}

}
