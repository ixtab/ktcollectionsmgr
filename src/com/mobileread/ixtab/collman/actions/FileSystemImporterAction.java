package com.mobileread.ixtab.collman.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.mobileread.ixtab.collman.I18n;
import com.mobileread.ixtab.collman.actions.sync.FileSystemImporter;

public class FileSystemImporterAction extends AbstractAction {
	private static final long serialVersionUID = 1L;
	
	public FileSystemImporterAction() {
		super(I18n.get().i18n(I18n.UPDATE_DIR_MENU_KEY, I18n.UPDATE_DIR_MENU_VALUE));
	}
	
	public void actionPerformed(ActionEvent e) {
		String msg = I18n.get().i18n(I18n.UPDATE_DIR_MESSAGE_KEY, I18n.UPDATE_DIR_MESSAGE_VALUE);
		String title = I18n.get().i18n(I18n.UPDATE_DIR_TITLE_KEY, I18n.UPDATE_DIR_TITLE_VALUE);
		if (KOptionPane.OK_OPTION == KOptionPane.showConfirmDialog(null, msg, title, KOptionPane.CANCEL_OK_OPTION)) {
			new FileSystemImporter().start();
		}
	}

}
