package com.mobileread.ixtab.collman.actions.sync;

import com.amazon.kindle.content.catalog.CatalogEntry;
import com.mobileread.ixtab.collman.adapters.CatalogService.QueryResultDepth;
import com.amazon.kindle.kindlet.ui.KOptionPane;
import com.mobileread.ixtab.collman.adapters.CatalogAdapter;
import com.mobileread.ixtab.collman.adapters.CatalogTransaction;
import com.mobileread.ixtab.collman.adapters.Predicate;
import com.mobileread.ixtab.collman.adapters.PredicateFactory;
import com.mobileread.ixtab.collman.catalog.Catalog;

public class AllCollectionsRemover {

	public static boolean run() {
		Predicate predicate = PredicateFactory.equals("type", "Collection");
		int chunks = 10;
		int[] total = new int[1];
		
		// this is weird: sometimes, it only removes part of the collections...
		boolean doItAgain = true;
		while (doItAgain) {
			for (int offset = 0; offset < Integer.MAX_VALUE; offset += chunks) {
				CatalogEntry[] entries = Catalog.find(predicate, true, offset, chunks, total, QueryResultDepth.FAST);
				if (offset == 0 && total[0] == 0) {
					doItAgain = false;
				}
				//Status.set("Deleting, collections left: "+total[0]);
				if (entries == null || entries.length == 0) {
					break;
				}
				CatalogTransaction t = Catalog.getBackend().openTransaction();
				for (int i=0; i < entries.length; ++i) {
					t.deleteEntry(CatalogAdapter.INSTANCE.getUUID(entries[i]));
				}
				if (!t.commitSync()) {
					KOptionPane.showMessageDialog(null, "Failed to remove existing collections.");
					return false;
				}
			}
		}
		//Status.set("Removed all existing collections.");
		return true;
	}

}
