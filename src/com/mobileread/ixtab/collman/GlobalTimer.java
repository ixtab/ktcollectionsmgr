package com.mobileread.ixtab.collman;

import java.util.Timer;

public class GlobalTimer {
	private static Timer timerInstance = null;
	
	public static Timer get() {
		if (timerInstance == null) {
			synchronized (GlobalTimer.class) {
				if (timerInstance == null) {
					timerInstance = new Timer(true);
				}
			}
		}
		return timerInstance;
	}
}
