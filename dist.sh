#!/bin/sh -e

# This file should be run from the root directory of the Collections Manager
# project. Don't expect it to work when running it from another directory.

PRODUCT=CollectionsManager
VERSION=`egrep 'product\.version' build.properties | cut -d '=' -f 2`
if [ "X$VERSION" = "X" ]; then
	echo "You're probably running this script from the wrong directory."
	echo "Look at the source code of $0 for more information."
	exit 1
fi

rm ${PRODUCT}_*.zip

ZIP=${PRODUCT}_${VERSION}.zip
zip -9 -r $ZIP LICENSE.txt *.azw2 *-help.png

echo ""
echo "Created: $ZIP"
